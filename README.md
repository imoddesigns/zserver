# zserver

ruby exercise 

to test post the following json data to the rest route /mapdrivers


{
  "1": {
    "name": "Ben Chillin",
    "id": 1,
    "location": {
      "lat": 29.673802,
      "long": -95753392,
      "address": "Long Prairie Trace, Richmond, TX 77407"
    },
    "orders": []
  },
  "2": {
    "name": "Bea Happy",
    "id": 2,
    "location": {
      "lat": 29.701817,
      "long": -95.760221,
      "address": "21711 Farm to Market 1093, Richmond, TX, 77407"
    },
    "orders": [
      {
        "id": 1,
        "origin": {
          "lat": 29.62271,
          "long": -95.647993,
          "address": "5520 Grand Parkway S, Richmond, TX 77406"
        },
        "destination": {
          "lat": null,
          "long": null,
          "address": "7702 Eastwood Lake Lane, Richmond Texas"
        },
        "size": 0
      },
      {
        "id": 3,
        "origin": {
          "lat": 29.663344,
          "long": -95.741297,
          "address": "7909 West Grand Parkway South, Richmond, TX 77407"
        },
        "destination": {
          "lat": 29.655374,
          "long": -95.695748,
          "address": "17727 Abermore Lane"
        },
        "size": 0
      }
    ]
  },
  "3": {
    "name": "Lar Dass",
    "id": 3,
    "location": {
      "lat": 29.639773,
      "long": -95.744019,
      "address": "Mason Road, Richmond, TX, 77406"
    },
    "orders": [
      {
        "id": 2,
        "origin": {
          "lat": 29.674558,
          "long": -95.753467,
          "address": "7035 West Grand Parkway South, Richmond, TX 77407"
        },
        "destination": {
          "lat": 29.676923,
          "long": -95.749421,
          "address": "5803 Eden Crest Court"
        },
        "size": 1
      }
    ]
  }
}