require 'spec_helper'

describe MapQuestGeoService do


  describe '#getDistances' do
    it 'should return a list of distances given an orgin location and a list of locations' do

      svc = MapQuestGeoService.new


      order = Order.new
      order.origin = Location.new 29.62271, -95.647993, "5520 Grand Parkway S, Richmond, TX 77406"
      order.size = 0

      driver1 = Driver.new 1, 'Chilly Willy'
      driver1.location = Location.new 29.743309, -95.77912, "23420 Cinco Ranch Boulevard Katy, TX"

      driver2 = Driver.new 2, 'Woody Woodpecker'
      driver2.location = Location.new 29.7424389035324, -95.7708753067337, "23211 Cinco Ranch Boulevard Katy, TX"

      driver3 = Driver.new 3, 'Wally Walrus'
      driver3.location = Location.new 29.833225, -95.745383, "3635 LAKES OF BRIDGEWATER DR Katy, TX"

      locations = [driver1.location, driver2.location, driver3.location]

      svc.get_distances_and_etas(order.origin, locations)


    end
  end

  describe 'MapApiDirectionsRequest' do

    describe '#to_json' do
      it 'should return the expected string' do


        loc1 = Location.new 29.62271, -95.647993, "5520 Grand Parkway S, Richmond, TX 77406"
        loc2 = Location.new 29.743309, -95.77912, nil

        request = MapQuestApiDirectionsRequest.new

        request.addLocation loc1
        request.addLocation loc2

        j = request.to_json

        r = r

      end

    end

  end
end