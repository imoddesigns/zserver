require 'spec_helper'

describe Driver do

  before :each do
    @driver = Driver.new "1", "james"

  end

  describe "#new" do
    it "return a new driver object" do
      expect(@driver).to be_an_instance_of Driver
    end
  end

  describe "#addOrder" do
    it "should add a new order to the orders collection" do
      order = Order.new
      @driver.add_order(order)

      expect(@driver.orders[0]).to be_an_instance_of Order
    end
  end

  describe '#can_handle_new_order' do
    it 'should return true when the driver has no orders' do

      @driver.clear_orders

      expect(@driver.can_handle_new_order).to be true

    end
  end

  describe '#can_handle_new_order' do
    it 'should return false when the driver has a large sized order' do

      @driver.clear_orders
      order = Order.new
      order.size = 1

      @driver.add_order order

      expect(@driver.can_handle_new_order).to be false

    end

  end

  describe '#can_handle_new_order' do
    it 'should return false when the driver has three orders' do

      @driver.clear_orders
      order1 = Order.new
      order2 = Order.new
      order3 = Order.new

      @driver.add_order order1
      @driver.add_order order2
      @driver.add_order order3


      expect(@driver.can_handle_new_order).to be false

    end

  end


end
