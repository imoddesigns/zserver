$: << File.expand_path('../..', __FILE__)

require 'zoom_api'
require 'models/location'
require 'models/driver'
require 'models/order'
require 'services/order_mapper'
require 'services/here_geoservice'
require './services/map_quest_geo_service'

require 'rack/test'

def app
  ZoomApi.new
end

RSpec.configure do |config|
  config.include Rack::Test::Methods
end
