require 'spec_helper'
require 'ostruct'

describe 'GeoCode' do


  class MOCKRESPONSE
    attr_accessor :code, :data
    def code
      @code
    end

    def data
      @data
    end

    def to_json
      return data
    end
  end

  class MOCKRESTCLIENT
    attr_accessor :mockResponse

    def mockResponse
      @mockResponse
    end
    def  get(url)

      return mockResponse

    end
  end

  before(:each) do

    @geoSvc = HereGeoService.new

  end

  describe '#makeGeoCodeRequest' do
    it 'should return a response message' do


      @geoSvc.restClient = generateMockGeoCodeRestClientWithSuccessStatusCode

      response = @geoSvc.send(:makeGeoCodeRequest, '7702 Eastwood Lake Lane, Richmond Texas')

      expect(response).not_to be nil
    end
  end


  describe '#makeReverseGeoCodeRequest' do
    it 'should return a response message' do

      @geoSvc.restClient = generateMockReverseGeoCodeRestClientWithSuccessStatusCode

      response = @geoSvc.send(:makeReverseGeoCodeRequest, 29.69981, -95.76975)

      expect(response).not_to be nil
    end
  end

  describe '#geoCode' do
    it 'should return best match for location' do

      @geoSvc.restClient = generateMockGeoCodeRestClientWithSuccessStatusCode

      location = @geoSvc.geoCode('7702 Eastwood Lake Lane, Richmond Texas')

      expect(location).to be_a Location

    end

  end

  describe '#reverseGeoCode' do
    it 'should return best match for location' do


      @geoSvc.restClient = generateMockReverseGeoCodeRestClientWithSuccessStatusCode
      location = @geoSvc.reverseGeoCode(29.69981, -95.76975)

      expect(location).to be_a Location
    end
  end


  def generateMockGeoCodeRestClientWithSuccessStatusCode

    data = File.open(File.dirname(__FILE__) + '/files/mockgeocode.data', 'r') { |file| file.read }
    mockGeoCodeResponse = MOCKRESPONSE.new
    mockGeoCodeResponse.code = 200



    mockGeoCodeResponse.data = data.gsub("\n", '')

    mockRestClient = MOCKRESTCLIENT.new

    mockRestClient.mockResponse = mockGeoCodeResponse

    return mockRestClient

  end

  def generateMockReverseGeoCodeRestClientWithSuccessStatusCode

    data = File.open(File.dirname(__FILE__) + '/files/mockgeocode.data', 'r') { |file| file.read }
    mockGeoCodeResponse = MOCKRESPONSE.new
    mockGeoCodeResponse.code = 200



    mockGeoCodeResponse.data = data.gsub("\n", '')

    mockRestClient = MOCKRESTCLIENT.new

    mockRestClient.mockResponse = mockGeoCodeResponse

    return mockRestClient
  end
end
