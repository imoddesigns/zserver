require 'spec_helper'

require_relative '../helpers/serializers'

describe OrderMapper do


  describe '#get_nearest_drivers_etas_for_order' do
    it 'Should' do

      order = Order.new
      order.origin = Location.new 29.62271, -95.647993, "5520 Grand Parkway S, Richmond, TX 77406"
      order.size = 0

      driver1 = Driver.new 1, 'Chilly Willy'
      driver1.location = Location.new 29.743309, -95.77912, "23420 Cinco Ranch Boulevard, Katy, TX"

      driver2 = Driver.new 2, 'Woody Woodpecker'
      driver2.location = Location.new 29.7424389035324, -95.7708753067337, "23211 Cinco Ranch Boulevard, Katy, TX"

      driver3 = Driver.new 3, 'Wally Walrus'
      driver3.location = Location.new 29.833225, -95.745383, "3635 LAKES OF BRIDGEWATER DR, Katy ,TX"


      drivers = [driver1, driver2, driver3]

      om = OrderMapper.new

      nearestDrivers = om.get_nearest_drivers_etas_for_order(order, drivers, 5)

      expect(nearestDrivers['drivers']).to contain_exactly(driver1, driver2)
    end
  end

  describe '#map_orders_to_drivers' do
    it 'should efficiently map orders to drivers' do

      orderBuffaloWildWings = Order.new
      orderBuffaloWildWings.origin = Location.new 29.62271, -95.647993, "5520 Grand Parkway S, Richmond, TX 77406"
      orderBuffaloWildWings.destination = Location.new nil, nil, '7702 Eastwood Lake Lane, Richmond Texas'
      orderBuffaloWildWings.size = 0

      orderWingStop = Order.new
      orderWingStop.origin = Location.new 29.674558, -95.753467, '7035 West Grand Parkway South, Richmond, TX 77407'
      orderWingStop.destination = Location.new 29.676923, -95.749421, '5803 Eden Crest Court'
      orderWingStop.size = 1

      orderLittleCaesars = Order.new
      orderLittleCaesars.origin = Location.new 29.663344, -95.741297, '7909 West Grand Parkway South, Richmond, TX 77407'
      orderLittleCaesars.destination = Location.new 29.655374, -95.695748, '17727 Abermore Lane'
      orderLittleCaesars.size = 0


      driver1 = Driver.new 1, 'Ben Chillin'
      driver1.location = Location.new 29.673802, -95753392, 'Long Prairie Trace, Richmond, TX 77407'

      driver2 = Driver.new 2, 'Bea Happy'
      driver2.location = Location.new 29.701817, -95.760221, '21711 Farm to Market 1093, Richmond, TX, 77407'

      driver3 = Driver.new 3, 'Lar Dass'
      driver3.location = Location.new 29.639773, -95.744019, 'Mason Road, Richmond, TX, 77406'

      mapper = OrderMapper.new

      drivers = [driver1, driver2, driver3]
      orders = [orderBuffaloWildWings, orderWingStop, orderLittleCaesars]

      #drivers_j = DriverSerializer.serialize(drivers, is_collection: true)

      drivers_str = drivers.to_json.gsub('\\', '')
      orders_str = orders.to_json.gsub '\\', ''
      ds= mapper.map_orders_to_drivers drivers, orders

      j = ds

    end
  end

end