require 'interface'

#defines methods for geoservice
GeoServiceInterface = interface{

  required_methods :geoCode, :reverseGeoCode, :distance


}