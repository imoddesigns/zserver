require 'httparty'
require 'ostruct'

class MapQuestGeoService


  def initialize
    @app_key = "PVzjwlphevu51z9fJ0HJVdSvrGLjYmVr"
    @base_geo_code_url = "http://www.mapquestapi.com/geocoding/v1/batch?key=#{@app_key}"
    @base_dir_url = "http://www.mapquestapi.com/directions/v2/routematrix?key=#{@app_key}"
  end

  def geoCode(addresses)


  end

  def reverseGeoCode(addreses)

  end

  def get_distances_and_etas(startingLocation, locations)

    request = MapQuestApiDirectionsRequest.new

    request.addLocation startingLocation

    for loc in locations
      request.addLocation loc
    end

    url = "#{@base_dir_url}"

    content = {

        :body => JSON.dump({"locations" => request.locations, "options" => {"allToAll" => false}}),
        :headers => {"Content-Type" => "application/json"}
    }

    response = HTTParty.post(url, content)

    result = JSON.parse(response.to_json, object_class: OpenStruct);

    if (result.info.statuscode != nil || result.info.statuscode == 0)


      mqDistances = result.distance;
      mqTimes = result.time;


      distances = mqDistances.slice(1, mqDistances.length - 1)
      times = mqTimes.slice(1, mqTimes.length - 1)
      v = {'distances' => distances, 'etas' => times}

      return v

    end


    raise StandardError

  end

  def getLocationAsParam(location)


  end

  #implements GeoServiceInterface
end


class MapQuestApiDirectionsRequest
  attr_accessor :locations,

      def locations
        @locations
      end

  def initialize()
    @locations = []
  end


  def addLocation(location)
    if (location.address == nil)
      latLng = Hash["latLng" => Hash["lat" => location.lat,
                                     "lng" => location.long]]

      locations.push(latLng)
    else
      locations.push(location.address)

    end


    def to_hash
      locHash = Hash['locations' => locations, 'options' => Hash['allToAll' => false]]
      return locHash
    end

    def to_json

      locHash = Hash['locations' => locations, 'options' => Hash['allToAll' => false]]

      return locHash.to_json

    end

  end

end
