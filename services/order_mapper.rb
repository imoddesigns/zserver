require_relative 'map_quest_geo_service'

class OrderMapper

  def map_orders_to_drivers(drivers, orders)


    driverHash = Hash.new

    #store drivers in hash for easy access
    for driver in drivers

      driverHash[driver.id] = driver

    end

    for order in orders

      #get the drivers nearest the order within 5 miles radius
      nearest_data = get_nearest_drivers_etas_for_order(order, drivers, 5)


      #iterate through drivers and assign order to first driver that meets all of the conditions
      #condition: driver must not already have a large order
      #condition: cannot add a large order when driver already has orders
      #condition: driver must not already have more than three orders

      for close_driver in nearest_data['drivers']


        driver = driverHash[close_driver.id]

        if driver.can_handle_new_order

          #don't add large order when driver already has orders
          if order.size === 1 && driver.get_order.length > 0
            next
          end

          driver.add_order order

          break
        end
      end

    end

    return driverHash;

  end

  def get_nearest_drivers_etas_for_order(order, drivers, radius)

    if drivers == nil || drivers.length < 1
      return []
    end


    if order == nil

      raise StandardError 'order should not be null'
    end


    locations = []
    closest_drivers = []
    driver_distances = []
    driver_etas = []

    for driver in drivers

      locations.push(driver.location)
    end

    distances_and_etas = MapQuestGeoService.new.get_distances_and_etas(order.origin, locations)

    drivers.each_with_index { |item, index|

      if distances_and_etas['distances'][index] <= radius

        closest_drivers.push(drivers[index])
        driver_distances.push(distances_and_etas['distances'][index])
        driver_etas.push(distances_and_etas['etas'][index])

      end

    }

    v = {"drivers" => closest_drivers, "distances" => driver_distances, 'etas' => driver_etas}

    return v

  end

# def distance_between(lat1, lon1, lat2, lon2)
#   rad_per_deg = Math::PI / 180
#   rm = 6371000 # Earth radius in meters
#
#   lat1_rad, lat2_rad = lat1 * rad_per_deg, lat2 * rad_per_deg
#   lon1_rad, lon2_rad = lon1 * rad_per_deg, lon2 * rad_per_deg
#
#   a = Math.sin((lat2_rad - lat1_rad) / 2) ** 2 + Math.cos(lat1_rad) * Math.cos(lat2_rad) * Math.sin((lon2_rad - lon1_rad) / 2) ** 2
#   c = 2 * Math::atan2(Math::sqrt(a), Math::sqrt(1 - a))
#
#   (rm * c) * 0.000621371 # Delta in meters converted to miles
# end

end
