require 'rest-client'
require 'json'
require 'httparty'
require 'ostruct'
require_relative 'geoservice'
require_relative '../errors/geo_code_error'
require_relative '../models/location'

class HereGeoService

  attr_writer :restClient

  def initialize()

    @_geo_code_base_url = 'https://geocoder.cit.api.here.com/6.2/geocode.json'
    @_reverse_geo_code_base_url = 'https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json'
    @_app_id = 'zY4jryJdLzExozY5d0zS'
    @_app_code = 'TGBOkFFgm8X5aZSpsW1aSA'
    @_get_with_params = "?app_id =#{@_app_id}&app_code=#{@_app_code}&gen=9"
  end


  def restClient
    #define default client as HTTParty so that we can inject a mock client in testing
    @restClient ||= HTTParty
  end

  def geoCode(address)

    geoResponse = makeGeoCodeRequest(address);

    location = processResponseLocation(geoResponse);

    return location

  end

  def reverseGeoCode(lat, long)

    geoResponse = makeReverseGeoCodeRequest(lat, long)

    location = processResponseLocation(geoResponse);

    return location
  end


  def distance()

  end


  protected
    def makeGeoCodeRequest(address)

      url = "#{@_geo_code_base_url}#{@_get_with_params}&searchtext=#{address}"

      response = restClient.get(url)

      if response.code === 200

        result = JSON.parse(response.to_json, object_class: OpenStruct)

        return result;
      end
    end

    protected
    def makeReverseGeoCodeRequest(lat, long)

      url = "#{@_reverse_geo_code_base_url}#{@_get_with_params}&prox=#{lat},#{long}&mode=retrieveAddresses"
      response = restClient.get(url)

      if response.code === 200

        result = JSON.parse(response.to_json, object_class: OpenStruct)

        return result;

      end
    end

  private
  def processResponseLocation(response)

    if (response.Response.View[0] === nil)
      raise GeoCodeError
    end

    result = response.Response.View[0].Result;

    #attempt to locate best match
    if (result != nil)

      #house match
      bestMatch = result.find { |r| r['MatchLevel'] === 'houseNumber' }

      if (bestMatch == nil)

        bestMatch = result.find { |r| r['MatchLevel'] === 'intersection' }

        if (bestMatch == nil)

          bestMatch = result.find { |r| r['MatchLevel'] === 'street' }


          if (bestMatch == nil)
            bestMatch = result.find { |r| r['MatchLevel'] === 'postalCode' }
          end

          if (bestMatch == nil)
            bestMatch = result.find { |r| r['MatchLevel'] === 'landmark' }
          end

          if (bestMatch == nil)
            bestMatch = result.find { |r| r['MatchLevel'] === 'district' }
          end

          if (bestMatch == nil)

            bestMatch = result.find { |r| r['MatchLevel'] === 'city' }
          end

          if (bestMatch == nil)
            bestMatch = result.find { |r| r['MatchLevel'] === 'county' }
          end

          if (bestMatch == nil)
            bestMatch = result.find { |r| r['MatchLevel'] === 'state' }
          end
        end

      end


      #we couldn't find a good match
      if (bestMatch == nil)

        raise GeoCodeError
      end

      ll = bestMatch.Location.NavigationPosition[0]
      address = bestMatch.Location.Address.Label


      location = Location.new ll.Latitude, ll.Longitude, address

      return location
    end

    implements GeoServiceInterface
  end
end




