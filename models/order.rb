class Order

  attr_accessor :id, :origin, :destination, :size, :estimated_pickup, :estimated_delivery

  def initialize
    @id = nil
    @size = 0
    @origin = nil
    @destination = nil
    @estimated_pickup = nil #estimated time for order pickup
    @estimated_delivery = nil #estimated time for order fulfillment
  end

  def as_json(options={})
    {

        id: @id,
        origin: @origin,
        destination: @destination,
        size: @size
    }
  end

  def to_json(*options)
    as_json(*options).to_json(*options)
  end
end