class Location
  attr_accessor :lat, :long, :address

  def initialize(lat, long, address)
    @lat = lat
    @long = long
    @address = address
  end

  def as_json(options={})
    {

        lat: @lat,
        long: @long,
        address: @address
    }
  end

  def to_json(*options)
    as_json(*options).to_json(*options)
  end
end