class Driver

  attr_accessor :id, :name, :location, :orders

   def initialize(id, name)
   @id = id
   @name = name
   @location = nil
   @orders = []
   @etas = []
   end

  def get_order

    return @orders
  end

  def add_order(order)
   @orders.push(order)
  end

  def clear_orders
    @orders = []
  end

  def can_handle_new_order

    if orders.length < 1
      return true
    end

    if orders.length == 3
      return false
    end

    lg_order = orders.find { |o| o.size === 1 }

    return lg_order == nil

  end

  def as_json(options={})
    {
        name: @name,
        id: @id,
        location: @location,
        orders: @orders
    }
  end

  def to_json(*options)
    as_json(*options).to_json(*options)
   end
end