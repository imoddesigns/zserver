require 'sinatra/base'
require 'json'
require 'ostruct'
require_relative 'models/driver'
require_relative 'models/location'
require_relative 'models/order'
require_relative 'services/order_mapper'


class ZoomApi < Sinatra::Base
  get '/' do
    'Hello World!'
  end

  post '/mapdrivers' do

    payload = JSON.parse(request.body.read.to_s, object_class: OpenStruct)


    if !able_to_prevalidate_request payload
      status 400
      return
    end

    posted_drivers = []

    for posted_driver in payload.drivers


      if !able_to_validate_posted_driver posted_driver

        status 400
        return
      end

      driver = Driver.new posted_driver.id, posted_driver.name

      loc = posted_driver.location

      driver.location = Location.new loc.lat, loc.long, loc.address

      posted_drivers.push(driver)
    end


    posted_orders = []

    for posted_order in payload.orders

      if !able_to_validate_posted_order posted_order
        status 400
        return
      end

      order = Order.new
      order.id = posted_order.id

      origin = posted_order.origin
      dest = posted_order.destination

      order.origin = Location.new origin.lat, origin.long, origin.address
      order.destination = Location.new dest.lat, dest.long, dest.address
      order.size = posted_order.size

      posted_orders.push(order)


    end


    begin

      mapper = OrderMapper.new

      response = mapper.map_orders_to_drivers(posted_drivers, posted_orders)

      content_type :json

      response.to_json

    rescue StandardError
      status 500

    end


  end
end


def able_to_prevalidate_request (request)
  if request.drivers == nil || request.orders == nil
    return false
  end

  return true
end

def able_to_validate_posted_driver (driver)

  if driver.id == nil || driver.name == nil || driver.location == nil

    return false

    if driver.location.lat == nil && driver.location.long == nill && driver.location.address == nil
      return false
    end
  end

  return true
end

def able_to_validate_posted_order (order)
  if order.id === nil || order.origin === nil || order.destination === nil
    return false
  end

  if (order.origin.lat === nil && order.origin.long === nil && order.origin.address === nil)
    return false
  end

  if (order.destination.lat === nil && order.destination.long === nil && order.destination.address == nil)
    return false
  end

  return true
end
